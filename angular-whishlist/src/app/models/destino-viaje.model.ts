import {v4 as unid} from 'uuid';

export class DestinoViaje {
  selected: boolean;
  servicios: string[];
  id = uuid();
  constructor(public nombre: string, public imagenUrl:string, public votes: number = 0) {
       this.servicios = ['pileta', 'desayuno'];
  }  
  SetSelected(s:boolean){
    this.selected = s;
  }
  isSelected(){
    return this.selected;
    }
    voteUp() {}
    setSelected(s: boolean) {
      this.votes++;
    }
    voteDown() {}
    setSelected(s: boolean) {
      this.votes--;
    }
}


